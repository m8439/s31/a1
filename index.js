 const express = require ('express');
 const mongoose = require ('mongoose');
 const dotenv = require ('dotenv');
 dotenv.config();

 const app = express();
 const PORT = 3006;


//connect routes to index.js file
const taskRoutes = require ('./routes/taskRoutes');
//Middlewares
app.use(express.json())
app.use(express.urlencoded({extened:true}))

//mongoose connection
//mongoose.connect (<connection.string, {options})

    mongoose.connect(process.env.MONGO_URL, {useNewUrlParser: true, useUnifiedTopology: true});
//DB connection notification

const db = mongoose.connection
db.on ("error", console.error.bind(console, 'connection errot'))
db.once("open", () => console.log(`Connected to database`))


//SCHEMA
    //moved the schema to model folder


//routes
//http://localhost:3006/api/task
app.use(`/api/task`, taskRoutes)

// Getting all the tasks

// Business Logic
/*
1. Retrieve all the documents
2. If an error is encountered, print the error
3. If no errors are found, send a success status back to the client/Postman and return an array of documents
*/



    
 app.listen(PORT,()=> console.log(`Server connected at PORT ${PORT}`) )