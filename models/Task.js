const mongoose = require('mongoose')


//create a schema for task
    //schema determines the structure of the documents to be written in the database
    //schema acts as a blueprint to our data
const taskSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: [true, `Name is required`]
        },
        status: {
            type: String,
            default: "pending"
        }
    }
)

//create a model out of the schema
    //syntax: mongoose.model(<name of the model>,<schema model came from>)
module.exports = mongoose.model(`Task`, taskSchema)
    //model is a programming interface that enables us to query and manipulate database using methods